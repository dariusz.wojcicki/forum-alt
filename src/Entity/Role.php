<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 */
class Role
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $name;

    /**
     * @ORM\OneToOne(targetEntity=UserRoles::class, mappedBy="role", cascade={"persist", "remove"})
     */
    private UserRoles $userRoles;

    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUserRoles(): ?UserRoles
    {
        return $this->userRoles;
    }

    public function setUserRoles(UserRoles $userRoles): self
    {
        // set the owning side of the relation if necessary
        if ($userRoles->getRole() !== $this) {
            $userRoles->setRole($this);
        }

        $this->userRoles = $userRoles;

        return $this;
    }
}
