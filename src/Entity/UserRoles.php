<?php

namespace App\Entity;

use App\Repository\UserRolesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Id;

/**
 * @ORM\Entity(repositoryClass=UserRolesRepository::class)
 */
class UserRoles
{
    /**
     * @Id
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="role", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private User $user;

    /**
     * @Id
     * @ORM\OneToOne(targetEntity=Role::class, inversedBy="userRoles", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private Role $role;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(Role $role): self
    {
        $this->role = $role;

        return $this;
    }
}
